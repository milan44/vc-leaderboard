module legacy-spam

go 1.15

require (
	github.com/bwmarrin/discordgo v0.23.2
	github.com/subosito/gotenv v1.2.0
	gitlab.com/milan44/logger v1.1.7
)

package main

import (
	"encoding/json"
	"errors"
	"github.com/subosito/gotenv"
	"io/ioutil"
	"os"
	"strings"
	"sync"
	"time"
)

const ActivityFile = "./activity.json"

var (
	BotToken   string
	GuildID    string
	VCChannels []string

	CurrentVCUsers = make(map[string]int64)
	UserActivity   = make(map[int64]map[string]int64)

	ActivityMutex sync.Mutex
)

func loadConfig() error {
	err := gotenv.Load(".env")
	if err != nil {
		return errors.New("failed to load .env")
	}

	BotToken = strings.TrimSpace(os.Getenv("BotToken"))
	GuildID = strings.TrimSpace(os.Getenv("GuildID"))
	VCChannels = strings.Split(strings.TrimSpace(os.Getenv("VCChannels")), ",")

	if BotToken == "" {
		return errors.New("'BotToken' not set in .env")
	} else if len(VCChannels) == 0 {
		return errors.New("'VCChannels' not set in .env")
	} else if GuildID == "" {
		return errors.New("'GuildID' not set in .env")
	}

	err = loadActivity()
	if err != nil {
		return errors.New("failed to load activity file")
	}

	return nil
}

func loadActivity() error {
	if _, err := os.Stat(ActivityFile); err == nil {
		b, err := ioutil.ReadFile(ActivityFile)
		if err != nil {
			return err
		}

		err = json.Unmarshal(b, &UserActivity)

		return err
	}

	return nil
}

func storeActivity(forceUpdate bool) {
	if forceUpdate {
		now := time.Now().Unix()
		day := getCurrentDay()

		ActivityMutex.Lock()

		_, ok := UserActivity[day]
		if !ok {
			UserActivity[day] = make(map[string]int64)
		}

		for user, enterTime := range CurrentVCUsers {
			spent := now - enterTime

			UserActivity[day][user] += spent
		}

		CurrentVCUsers = make(map[string]int64)

		ActivityMutex.Unlock()

		getLeaderboard()
	}

	b, _ := json.Marshal(UserActivity)

	_ = ioutil.WriteFile(ActivityFile, b, 0777)
}

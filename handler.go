package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"strconv"
	"strings"
	"time"
)

func OnMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	if s == nil || m == nil {
		return
	}

	if m.GuildID != GuildID {
		return
	}

	if m.Content == "tleaderboard" {
		leaderboard := getLeaderboard()

		lines := make([]string, len(leaderboard))

		max := 0

		for _, entry := range leaderboard {
			l := len([]rune(entry.GetUserName(s)))

			if l > max {
				max = l
			}
		}

		for i, entry := range leaderboard {
			if i > 4 && entry.User != m.Author.ID {
				continue
			}

			lines[i] = fmt.Sprintf("%d. %-"+strconv.Itoa(max)+"s -> %s", i+1, entry.GetUserName(s), formatSeconds(entry.Time))
		}

		_, _ = s.ChannelMessageSend(m.ChannelID, "**__Leaderboard__**\n```\n"+strings.Join(lines, "\n")+"\n```")
	}
}

func OnVoiceStateChange(s *discordgo.Session, v *discordgo.VoiceStateUpdate) {
	if s == nil || v == nil {
		return
	}

	if v.GuildID != GuildID {
		return
	}

	day := getCurrentDay()
	user := v.UserID

	if isValidVoiceChannel(v.ChannelID) {
		ActivityMutex.Lock()
		CurrentVCUsers[user] = time.Now().Unix()
		ActivityMutex.Unlock()

		log.Debug(user + " joined vc")
	} else {
		ActivityMutex.Lock()
		joined, ok := CurrentVCUsers[user]
		ActivityMutex.Unlock()

		if ok {
			spent := time.Now().Unix() - joined

			ActivityMutex.Lock()

			_, ok := UserActivity[day]
			if !ok {
				UserActivity[day] = make(map[string]int64)
			}

			UserActivity[day][user] += spent

			delete(CurrentVCUsers, user)

			ActivityMutex.Unlock()

			storeActivity(false)

			log.Debug(user + " left vc")
		}
	}
}

package main

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/milan44/logger"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	log = logger.NewGinStyleLogger(false)
)

func main() {
	rand.Seed(time.Now().UnixNano())

	log.Info("Loading Config...")
	err := loadConfig()
	log.MustPanic(err)

	log.Info("Connecting...")
	discord, err := discordgo.New("Bot " + BotToken)
	log.MustPanic(err)

	discord.AddHandler(OnMessage)
	discord.AddHandler(OnVoiceStateChange)
	discord.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildVoiceStates | discordgo.IntentsGuildMessages)

	err = discord.Open()
	log.MustPanic(err)

	err = discord.UpdateListeningStatus("Epic dev streams")
	if err != nil {
		log.Warning("Unable to update status: " + err.Error())
	}

	log.Info("Startup completed.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	log.Info("Persisting activity data...")

	storeActivity(true)

	log.Warning("Disconnecting...")

	_ = discord.Close()
}

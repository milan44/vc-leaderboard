package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"math"
	"sort"
	"strings"
	"sync"
	"time"
)

type LeaderBoardEntry struct {
	User string
	Time int64

	_username string
}

var (
	userNameCache      = make(map[string]string)
	userNameCacheMutex sync.Mutex
)

func getCurrentDay() int64 {
	date := time.Now().UTC().Format("2006-01-02")

	t, _ := time.Parse("2006-01-02", date)

	return t.UTC().Unix()
}

func getLeaderboard() []LeaderBoardEntry {
	oldest := getCurrentDay() - (60 * 60 * 24 * 5)

	ActivityMutex.Lock()

	for day := range UserActivity {
		if day < oldest {
			delete(UserActivity, day)
		}
	}

	allUsers := make(map[string]int64)

	for _, users := range UserActivity {
		for user, spent := range users {
			allUsers[user] += spent
		}
	}

	ActivityMutex.Unlock()

	leaderboard := make([]LeaderBoardEntry, 0)

	for user, spent := range allUsers {
		leaderboard = append(leaderboard, LeaderBoardEntry{
			User: user,
			Time: spent,
		})
	}

	sort.Slice(leaderboard, func(i, j int) bool {
		return leaderboard[i].Time > leaderboard[j].Time
	})

	return leaderboard
}

func (l *LeaderBoardEntry) GetUserName(s *discordgo.Session) string {
	userNameCacheMutex.Lock()
	name, ok := userNameCache[l.User]
	userNameCacheMutex.Unlock()

	if !ok {
		user, err := s.User(l.User)

		if err != nil {
			name = "@Unknown#0000"
		} else {
			name = "@" + user.Username + "#" + user.Discriminator
		}

		userNameCacheMutex.Lock()
		userNameCache[l.User] = name
		userNameCacheMutex.Unlock()
	}

	return name
}

func isValidVoiceChannel(channelId string) bool {
	ActivityMutex.Lock()

	for _, channel := range VCChannels {
		if channel == channelId {
			ActivityMutex.Unlock()

			return true
		}
	}

	ActivityMutex.Unlock()

	return false
}

func formatSeconds(seconds int64) string {
	s := float64(seconds)

	minutes := math.Floor(s / 60)
	s -= minutes * 60

	hours := math.Floor(minutes / 60)
	minutes -= hours * 60

	days := math.Floor(hours / 24)
	hours -= days * 24

	years := math.Floor(days / 365)
	days -= years * 365

	elements := make([]string, 0)

	if years > 0 {
		elements = append(elements, fmt.Sprintf("%.0f years", years))
	}
	if days > 0 {
		elements = append(elements, fmt.Sprintf("%.0f days", days))
	}
	if hours > 0 {
		elements = append(elements, fmt.Sprintf("%.0f hours", hours))
	}
	if minutes > 0 {
		elements = append(elements, fmt.Sprintf("%.0f mins", minutes))
	}
	if s > 0 || len(elements) == 0 {
		elements = append(elements, fmt.Sprintf("%.0f secs", s))
	}

	if len(elements) == 1 {
		return elements[0]
	}

	last := elements[len(elements)-1]

	return strings.Join(elements[:len(elements)-1], ", ") + " and " + last
}

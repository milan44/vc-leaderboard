@echo off

echo Building Linux...
set GOOS=linux
go build -o dist/vc-leaderboard

echo Building Windows...
set GOOS=windows
go build -o dist/vc-leaderboard.exe